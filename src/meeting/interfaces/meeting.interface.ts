import { Document } from "mongoose";

export interface Meeting extends Document {
    readonly date: Date,
    readonly meetingState: String
}