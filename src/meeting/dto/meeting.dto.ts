export class MeetingDto {
    readonly date: Date;
    readonly meetingState: string
}