import * as mongoose from 'mongoose';

export const MeetingSchema = new mongoose.Schema({
    date: Date,
    meetingState: {
        type: String,
        enum: ['NOT_STARTED', 'STARTED', 'COMPLETED'],
        default: 'NOT_STARTED'
    },
});