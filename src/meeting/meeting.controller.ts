import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { MeetingService } from './meeting.service';
import { get } from 'mongoose';
import { Meeting } from './interfaces/meeting.interface';
import { MeetingDto } from './dto/meeting.dto';

@Controller('meeting')
export class MeetingController {
    constructor(private meetingService: MeetingService){ }

    @Get('/read-all')
    async readAll(): Promise<Meeting[]> {
        return await this.meetingService.findAll();
    }

    @Get('/read-all-not-started')
    async notStarted(): Promise<Meeting[]> {
        return await this.meetingService.findNotStarted();
    }

    @Get('/read-all-started')
    async Started(): Promise<Meeting[]> {
        return await this.meetingService.findStarted();
    }

    @Get('/read-all-completed')
    async completed(): Promise<Meeting[]> {
        return await this.meetingService.findCompleted();
    }

    @Get('/read/:id')
    async readById(@Param('id') id: string ): Promise<Meeting> {
        return await this.meetingService.findById(id);
    }

    @Post('/create')
    async create(@Body() data: MeetingDto): Promise<void> {
        return await this.meetingService.create(data);
    }
}
