import { Injectable, ConflictException, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Meeting } from './interfaces/meeting.interface';
import { MeetingDto } from './dto/meeting.dto';

@Injectable()
export class MeetingService {
    constructor(@InjectModel('Meeting') private meetingModel: Model<Meeting>) { }

    async findAll(): Promise<Meeting[]> {
        return await this.meetingModel.find().exec();
    }

    async findNotStarted(): Promise<Meeting[]> {
        return await this.meetingModel.find().where('meetingState', 'NOT_STARTED').exec();
    }

    async findStarted(): Promise<Meeting[]> {
        return await this.meetingModel.find().where('meetingState', 'STARTED').exec();
    }

    async findCompleted(): Promise<Meeting[]> {
        return await this.meetingModel.find().where('meetingState', 'COMPLETED').exec();
    }

    async findById(id: string): Promise<Meeting> {
        const meeting = await this.meetingModel.findById(id);
        if (!meeting) {
            throw new HttpException('', HttpStatus.NOT_FOUND);
        }
        return meeting;
    }

    async create(data: MeetingDto): Promise<void> {
        let meeting = await this.meetingModel.findOne(data);

        if (meeting) {
            throw new HttpException('Sorry this meeting already exists', HttpStatus.FOUND);
        }

        meeting = await this.meetingModel.create(data);

        try {
            await meeting.save();
        } catch (error) {
            throw error;
        }
    }

    async update(id: string, data: MeetingDto): Promise<Meeting> {
        let meeting = await this.meetingModel.findById(id);

        if (!meeting) {
            throw new HttpException('', HttpStatus.NOT_FOUND)
        }

        await meeting.update(data);
        return meeting;
    }
}
