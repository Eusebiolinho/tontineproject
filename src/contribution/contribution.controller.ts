import { Controller, Post, Param, Body } from '@nestjs/common';
import { ContributionService } from './contribution.service';
import { ContributionDto } from './dto/contribution.dto';

@Controller('contribution')
export class ContributionController {
    constructor(private contributionService: ContributionService){}

    @Post('/create/:id')
    async create(@Param('id') ids: String, @Body() data: ContributionDto): Promise<void> {
        return await this.contributionService.create(ids, data);
    }
}
