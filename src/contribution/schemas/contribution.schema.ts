import * as mongoose from 'mongoose';

export const ContributionSchema = new mongoose.Schema({
    adherent: Object,
    meeting: Object,
    amount: Number,
    created: { type: Date, default: Date.now }
});