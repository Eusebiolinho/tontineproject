import { Document } from "mongoose";

export interface Contribution extends Document {
    readonly meeting: Object,
    readonly adherent: Object,
    readonly amount: Number
}