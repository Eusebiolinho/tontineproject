import { Module } from '@nestjs/common';
import { ContributionService } from './contribution.service';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AdherentModule } from 'src/adherent/adherent.module';
import { MeetingModule } from 'src/meeting/meeting.module';
import { ContributionSchema } from './schemas/contribution.schema';
import { ContributionController } from './contribution.controller';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forFeature([{ name: 'Contribution', schema: ContributionSchema }]),
    AdherentModule,
    MeetingModule
  ],
  providers: [ContributionService],
  controllers: [ContributionController]
})
export class ContributionModule { }
