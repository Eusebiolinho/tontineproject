import { Injectable, Inject, Param, Body, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Contribution } from './interfaces/contribution.interface';
import { AdherentService } from 'src/adherent/adherent.service';
import { MeetingService } from 'src/meeting/meeting.service';
import { ContributionDto } from './dto/contribution.dto';

@Injectable()
export class ContributionService {
    constructor(
        @InjectModel('Contribution')
        private contributionModel: Model<Contribution>
    ) { }

    @Inject() private readonly adherentService: AdherentService;
    @Inject() private readonly meetingService: MeetingService;

    async create(ids: String, data: ContributionDto): Promise<void> {
        /* ids is a combination of 2 ids separated by a dash of 6 "-",
          in this case the meeting id and the member id. */

        const separator = ids.indexOf('-');
        const idAdherent = ids.substring(0, separator);
        const idMeeting = ids.substring((separator + 1), ids.length);
        
        const _meeting = await this.meetingService.findById(idMeeting);
        const _adherent = await this.adherentService.findOne(idAdherent);
        const _amount = data.amount;
        
        if (!_meeting || !_adherent) {
            throw new HttpException('', HttpStatus.NOT_FOUND);
        }

        const contribution = new this.contributionModel({
            meeting: _meeting,
            adherent: _adherent,
            amount: _amount
        });
        console.log(typeof(data.amount));
        console.log(contribution);
        try {
            await contribution.save();
        } catch (error) {
            throw error.starck;
        }
    }
}
