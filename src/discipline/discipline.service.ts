import { Injectable, HttpException, HttpStatus, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Discipline } from './interfaces/discipline.interface';
import { MeetingService } from 'src/meeting/meeting.service';
import { AdherentService } from 'src/adherent/adherent.service';
import { AttendenceDto } from './dto/disciplineDto';

@Injectable()
export class DisciplineService {
    constructor(@InjectModel('Discipline')
    private disciplineModel: Model<Discipline>,
    ) { }

    @Inject() private readonly adherentService: AdherentService;
    @Inject() private readonly meetingService: MeetingService;

    async createAttendence(ids: string): Promise<void> {
        const separator = ids.indexOf('-');
        const idAdherent = ids.substring(0, separator);
        const idMeeting = ids.substring((separator + 1), ids.length);

        const _meeting = await this.meetingService.findById(idMeeting);
        const _adherent = await this.adherentService.findOne(idAdherent);

        if (!_meeting || !_adherent) {
            throw new HttpException('', HttpStatus.NOT_FOUND);
        }

        const discipline = new this.disciplineModel({
            attendence: {
                meeting: _meeting,
                adherent: _adherent
            }
        });

        try {
            await discipline.save();
        } catch (error) {
            throw error.starck;
        }
    }

    async
}
