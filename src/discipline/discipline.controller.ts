import { Controller, Param, Post } from '@nestjs/common';
import { DisciplineService } from './discipline.service';

@Controller('discipline')
export class DisciplineController {
    constructor(private disciplineService: DisciplineService){}

    @Post('create-attendence/:id')
    async createAttendence(@Param('id') ids: string): Promise<void> {
        return await this.disciplineService.createAttendence(ids);
    }
}
