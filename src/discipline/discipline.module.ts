import { Module, forwardRef } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { DisciplineSchema } from './schemas/discipline.schema';
import { DisciplineController } from './discipline.controller';
import { DisciplineService } from './discipline.service';
import { AdherentModule } from 'src/adherent/adherent.module';
import { MeetingModule } from 'src/meeting/meeting.module';

@Module({
    imports: [
        ConfigModule.forRoot(),
        MongooseModule.forFeature([{ name: 'Discipline', schema: DisciplineSchema }]),
        AdherentModule,
        MeetingModule
    ],
    controllers: [DisciplineController],
    providers: [DisciplineService],
})
export class DisciplineModule { }
