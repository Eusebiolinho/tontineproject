import { Document } from "mongoose";

export interface Discipline extends Document {
    readonly attendences: {
        date: Date,
        state: Boolean,
        meeting: Object,
        adherent: Object
    }
}