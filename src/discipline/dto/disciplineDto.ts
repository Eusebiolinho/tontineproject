export class AttendenceDto {
    attendences: {
        state: boolean,
        meeting: object,
        adherent: object
    }
}