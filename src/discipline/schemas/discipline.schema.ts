import * as mongoose from 'mongoose';

export const DisciplineSchema = new mongoose.Schema({
    attendence: {
        date: { type: Date, default: Date.now },
        state: { type: Boolean, default: false },
        meeting: Object,
        adherent: Object,
    }
});