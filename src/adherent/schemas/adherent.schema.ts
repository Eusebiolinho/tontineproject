import * as mongoose from 'mongoose';

export const AdherentSchema = new mongoose.Schema({
    name: {
        first: {
            type: String,
            // lowercase: false,
            // required: true
        },
        last: {
            type: String,
            // required: true
        },
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    genre: String,
    role: String,
    created: {
        type: Date,
        default: Date.now
    },
    active: {
        type: Boolean,
        default: true
    }
});