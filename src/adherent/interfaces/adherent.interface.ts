import { Document } from "mongoose";

export interface Adherent extends Document {
    readonly name: {
        first: String,
        last: String
    },
    readonly email: String,
    readonly genre: String,
    readonly role: String,
    readonly created: Date,
    readonly active: Boolean
}