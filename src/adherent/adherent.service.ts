import { Injectable, ConflictException, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Adherent } from './interfaces/adherent.interface';
import { CreateAdherentDto, UpdateAdherentDto } from './dto/adherent.dto';

@Injectable()
export class AdherentService {
    constructor(@InjectModel('Adherent') private readonly adherentModel: Model<Adherent>) { }

    findAll(): Promise<Adherent[]> {
        return this.adherentModel.find().exec();
    }

    findAllAdherentsActive(): Promise<Adherent[]> {
        return this.adherentModel.find().where('active', true).exec();
    }

    findAllAdherentsDeleted(): Promise<Adherent[]> {
        return this.adherentModel.find().where('active', false).exec();
    }

    async findOne(id: string): Promise<Adherent> {
        const adherent = await this.adherentModel.findById(id);
        if (!adherent) {
            throw new HttpException('', HttpStatus.NOT_FOUND);
        }
        return adherent;
    }

    async create(data: CreateAdherentDto): Promise<void> {
        const adherent = await this.adherentModel.create(data);
        try {
            await adherent.save();
        } catch (error) {
            if (error.code === 11000) {
                throw new ConflictException('Adherent already exists');
            }
            throw error;
        }
    }

    async update(id: string, data: UpdateAdherentDto): Promise<Adherent> {
        let adherent = await this.adherentModel.findById(id);
        if (!adherent) {
            throw new HttpException('', HttpStatus.NOT_FOUND);
        }
        console.log(data);
        await adherent.updateOne(data);
        return adherent;
    }

    async delete(id: string): Promise<Adherent> {
        const adherent = await this.adherentModel.findById(id);
        if (!adherent) {
            throw new HttpException('', HttpStatus.NOT_FOUND);
        }
        const { name, email, genre, role, created, active } = adherent;
        let data = new UpdateAdherentDto(false);
        console.log(data)
        //await adherent.remove();
        await adherent.updateOne(data);
        return adherent;
    }
}
