export class CreateAdherentDto {
    readonly name: {
        first: String,
        last: String
    };
    readonly email: string;
    readonly genre: string;
    readonly role: string;
}

export class UpdateAdherentDto {
    constructor(_active){
        this.active = _active;
    };

    readonly name: {
        first: string,
        last: string
    };
    readonly email: string;
    readonly genre: string;
    readonly role: string;
    readonly active: boolean
}