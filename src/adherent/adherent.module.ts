import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AdherentController } from './adherent.controller';
import { AdherentService } from './adherent.service';
import { AdherentSchema } from './schemas/adherent.schema';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forFeature([{ name: 'Adherent', schema: AdherentSchema }])
  ],
  controllers: [AdherentController],
  providers: [AdherentService],
  exports: [AdherentService]
})
export class AdherentModule { }
