import { Controller, Get, Post, Put, Delete, Body, Req, Param } from '@nestjs/common';
import { AdherentService } from './adherent.service';
import { Adherent } from './interfaces/adherent.interface';
import { CreateAdherentDto, UpdateAdherentDto } from './dto/adherent.dto';

@Controller('adherent')
export class AdherentController {
    constructor(private adherentService:  AdherentService){}

    @Get('/read-all')
    readAll(): Promise<Adherent[]>{
        return this.adherentService.findAll();
    }

    @Get('/readall-adherents-active')
    readAllAdherentsActive(): Promise<Adherent[]>{
        return this.adherentService.findAllAdherentsActive();
    }

    @Get('/readall-adherents-deleted')
    readAllAdherentDeleted(): Promise<Adherent[]>{
        return this.adherentService.findAllAdherentsDeleted();
    }

    @Get('/read-one/:id')
    async readOne(@Param('id') id: string): Promise<Adherent>{
        return this.adherentService.findOne(id);
    }

    @Post('/create')
    async create(@Body() data: CreateAdherentDto): Promise<void>{
        return await this.adherentService.create(data);
    }

    @Put('/edite/:id')
    async edite(@Param('id') id: string, @Body() data: UpdateAdherentDto): Promise<Adherent>{
        return this.adherentService.update(id, data);
    }

    @Delete('/remove/:id')
    async remove(@Param('id') id: string): Promise<Adherent>{
        return this.adherentService.delete(id);
    }
}
