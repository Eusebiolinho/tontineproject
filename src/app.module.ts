import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AdherentModule } from './adherent/adherent.module';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { DisciplineModule } from './discipline/discipline.module';
import { MeetingModule } from './meeting/meeting.module';
import { ContributionModule } from './contribution/contribution.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }),
    AdherentModule,
    DisciplineModule,
    MeetingModule,
    ContributionModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
